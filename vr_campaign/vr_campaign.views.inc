<?php

/**
 * hook_views_data implementation
 */
function vr_campaign_views_data() {
  $data['vr_campaign_recipients']['table']['join']['users'] = array(
    'left_field' => 'uid',
    'field' => 'uid',
    );
  $data['vr_campaign_recipients']['vrid'] = array(
    'group' => t('VerticalResponse'),
    'title' => t('Campaign Id'),
    'help' => t('Campaign Id that has been assigned by VerticalResponse.'),
    'field' => array(
      'handler' => 'vr_campaign_field_campaign_id',
      ),
    'argument' => array(
      'hanlder' => 'views_handler_argument',
      ),
    'filter' => array(
      'help' => t('Filter Drupal users by VerticalResponse campaign id.'),
      'handler' => 'vr_campaign_filter_campaign_id',
      ),
    );
  $data['vr_campaign']['table']['join']['users'] = array(
    'left_table' => 'vr_campaign_recipients',
    'left_field' => 'vrid',
    'field' => 'vrid',
    );
  $data['vr_campaign']['nid'] = array(
    'group' => t('VerticalResponse'),
    'title' => t('Campaign Nid'),
    'help' => t('The nid of a campaign node.'),
    'argument' => array(
      'handler' => 'vr_campaign_argument_campaign_id',
      ),
    );
  return $data;
}

/**
 * hook_views handlers, add awesome handlers.
 */
function vr_campaign_views_handlers() {
  $path = drupal_get_path('module', 'vr_campaign') . '/handlers';
  return array(
    'info' => array(
      'path' => $path,
    ),
    'handlers' => array(
      'vr_campaign_filter_campaign_id' => array(
        'parent' => 'views_handler_filter_many_to_one',
        ),
      'vr_campaign_field_campaign_id' => array(
        'parent' => 'views_handler_field',
        ),
      'vr_campaign_argument_campaign_id' => array(
        'parent' => 'views_handler_argument',
        ),
      ),
    );
}

/**
 * Provide default views
 */
function vr_campaign_views_default_views() {
  $view = new view;
  $view->name = 'compile_list';
  $view->description = '';
  $view->tag = '';
  $view->view_php = '';
  $view->base_table = 'users';
  $view->is_cacheable = FALSE;
  $view->api_version = 2;
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */
  $handler = $view->new_display('default', 'Defaults', 'default');
  $handler->override_option('fields', array(
    'mail' => array(
      'label' => 'E-mail',
      'alter' => array(
        'alter_text' => 0,
        'text' => '',
        'make_link' => 0,
        'path' => '',
        'link_class' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'target' => '',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'html' => 0,
        'strip_tags' => 0,
      ),
      'empty' => '',
      'hide_empty' => 0,
      'empty_zero' => 0,
      'link_to_user' => 'user',
      'exclude' => 0,
      'id' => 'mail',
      'table' => 'users',
      'field' => 'mail',
      'relationship' => 'none',
    ),
    'name' => array(
      'label' => 'Name',
      'alter' => array(
        'alter_text' => 0,
        'text' => '',
        'make_link' => 0,
        'path' => '',
        'link_class' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'target' => '',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'html' => 0,
        'strip_tags' => 0,
      ),
      'empty' => '',
      'hide_empty' => 0,
      'empty_zero' => 0,
      'link_to_user' => 1,
      'overwrite_anonymous' => 0,
      'anonymous_text' => '',
      'exclude' => 0,
      'id' => 'name',
      'table' => 'users',
      'field' => 'name',
      'relationship' => 'none',
    ),
    'rid' => array(
      'label' => 'Roles',
      'alter' => array(
        'alter_text' => 0,
        'text' => '',
        'make_link' => 0,
        'path' => '',
        'link_class' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'target' => '',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'html' => 0,
        'strip_tags' => 0,
      ),
      'empty' => '',
      'hide_empty' => 0,
      'empty_zero' => 0,
      'type' => 'separator',
      'separator' => ', ',
      'exclude' => 0,
      'id' => 'rid',
      'table' => 'users_roles',
      'field' => 'rid',
      'relationship' => 'none',
    ),
  ));
  $handler->override_option('arguments', array(
    'nid' => array(
      'default_action' => 'ignore',
      'style_plugin' => 'default_summary',
      'style_options' => array(),
      'wildcard' => 'all',
      'wildcard_substitution' => 'All',
      'title' => '',
      'breadcrumb' => '',
      'default_argument_type' => 'fixed',
      'default_argument' => '',
      'validate_type' => 'none',
      'validate_fail' => 'not found',
      'id' => 'nid',
      'table' => 'vr_campaign',
      'field' => 'nid',
      'validate_user_argument_type' => 'uid',
      'validate_user_roles' => array(
        '2' => 0,
      ),
      'override' => array(
        'button' => 'Override',
      ),
      'relationship' => 'none',
      'default_options_div_prefix' => '',
      'default_argument_fixed' => '',
      'default_argument_user' => 0,
      'default_argument_php' => '',
      'validate_argument_node_type' => array(
        'enews' => 0,
        'page' => 0,
        'story' => 0,
      ),
      'validate_argument_node_access' => 0,
      'validate_argument_nid_type' => 'nid',
      'validate_argument_vocabulary' => array(),
      'validate_argument_type' => 'tid',
      'validate_argument_transform' => 0,
      'validate_user_restrict_roles' => 0,
      'validate_argument_php' => '',
      'apply' => 0,
    ),
  ));
  $handler->override_option('filters', array(
    'uid' => array(
      'operator' => 'not in',
      'value' => array(
        '0' => 0,
      ),
      'group' => '0',
      'exposed' => FALSE,
      'expose' => array(
        'operator' => FALSE,
        'label' => '',
      ),
      'id' => 'uid',
      'table' => 'users',
      'field' => 'uid',
      'relationship' => 'none',
    ),
  ));
  $handler->override_option('access', array(
    'type' => 'perm',
    'perm' => 'administer VerticalResponse',
  ));
  $handler->override_option('cache', array(
    'type' => 'none',
  ));
  $handler->override_option('title', 'Review Recipients');
  $handler->override_option('header_format', '1');
  $handler->override_option('header_empty', 0);
  $handler->override_option('empty', 'Please <a href="vr-recipients">select recipients</a> for this campaign.');
  $handler->override_option('empty_format', '1');
  $handler->override_option('use_ajax', TRUE);
  $handler->override_option('use_pager', '1');
  $handler->override_option('style_plugin', 'bulk');
  $handler->override_option('style_options', array(
    'grouping' => '',
    'override' => 1,
    'sticky' => 0,
    'order' => 'asc',
    'columns' => array(
      'mail' => 'mail',
      'name' => 'name',
      'rid' => 'rid',
    ),
    'info' => array(
      'mail' => array(
        'sortable' => 0,
        'separator' => '',
      ),
      'name' => array(
        'sortable' => 0,
        'separator' => '',
      ),
      'rid' => array(
        'separator' => '',
      ),
    ),
    'default' => '-1',
    'execution_type' => '2',
    'display_type' => '0',
    'hide_select_all' => 0,
    'skip_confirmation' => 1,
    'display_result' => 0,
    'merge_single_action' => 1,
    'selected_operations' => array(
      'vr_list_user_subscribe' => 'vr_list_user_subscribe',
      'user_block_ip_action' => 0,
      'user_block_user_action' => 0,
      'user_user_operations_block' => 0,
      'views_bulk_operations_delete_user_action' => 0,
      'system_message_action' => 0,
      'views_bulk_operations_action' => 0,
      'views_bulk_operations_script_action' => 0,
      'views_bulk_operations_user_roles_action' => 0,
      'views_bulk_operations_argument_selector_action' => 0,
      'system_goto_action' => 0,
      'vr_list_user_unsubscribe' => 0,
      'system_send_email_action' => 0,
      'user_user_operations_unblock' => 0,
    ),
  ));
  $handler = $view->new_display('page', 'Select Recipients', 'page_1');
  $handler->override_option('header', 'Use the form below to select campaign recipients.   Recipients can be removed during the <a href="vr-recipients-review">review process</a>.');
  $handler->override_option('empty', '');
  $handler->override_option('path', 'node/%/enews-export/vr-recipients');
  $handler->override_option('menu', array(
    'type' => 'tab',
    'title' => 'Select Recipients',
    'description' => 'Select recipients for a campaign',
    'weight' => '1',
    'name' => 'navigation',
  ));
  $handler->override_option('tab_options', array(
    'type' => 'none',
    'title' => '',
    'description' => '',
    'weight' => 0,
    'name' => 'navigation',
  ));
  $handler = $view->new_display('page', 'Review Recipients', 'page_2');
  $handler->override_option('arguments', array(
    'nid' => array(
      'default_action' => 'ignore',
      'style_plugin' => 'default_summary',
      'style_options' => array(),
      'wildcard' => 'all',
      'wildcard_substitution' => 'All',
      'title' => '',
      'breadcrumb' => '',
      'default_argument_type' => 'fixed',
      'default_argument' => '',
      'validate_type' => 'none',
      'validate_fail' => 'not found',
      'id' => 'nid',
      'table' => 'vr_campaign',
      'field' => 'nid',
      'validate_user_argument_type' => 'uid',
      'validate_user_roles' => array(
        '2' => 0,
      ),
      'override' => array(
        'button' => 'Use default',
      ),
      'relationship' => 'none',
      'default_options_div_prefix' => '',
      'default_argument_fixed' => '',
      'default_argument_user' => 0,
      'default_argument_php' => '',
      'validate_argument_node_type' => array(
        'enews' => 0,
        'page' => 0,
        'story' => 0,
      ),
      'validate_argument_node_access' => 0,
      'validate_argument_nid_type' => 'nid',
      'validate_argument_vocabulary' => array(),
      'validate_argument_type' => 'tid',
      'validate_argument_transform' => 0,
      'validate_user_restrict_roles' => 0,
      'validate_argument_php' => '',
      'apply' => 1,
    ),
  ));
  $handler->override_option('header', 'Use the form below to remove campaign recipients.   Recipients can be added during the <a href="vr-recipients">list selection process</a>.');
  $handler->override_option('style_options', array(
    'grouping' => '',
    'override' => 1,
    'sticky' => 0,
    'order' => 'asc',
    'columns' => array(
      'mail' => 'mail',
      'name' => 'name',
      'rid' => 'rid',
    ),
    'info' => array(
      'mail' => array(
        'sortable' => 0,
        'separator' => '',
      ),
      'name' => array(
        'sortable' => 0,
        'separator' => '',
      ),
      'rid' => array(
        'separator' => '',
      ),
    ),
    'default' => '-1',
    'execution_type' => '2',
    'display_type' => '0',
    'hide_select_all' => 0,
    'skip_confirmation' => 1,
    'display_result' => 0,
    'merge_single_action' => 1,
    'selected_operations' => array(
      'vr_list_user_unsubscribe' => 'vr_list_user_unsubscribe',
      'user_block_ip_action' => 0,
      'user_block_user_action' => 0,
      'user_user_operations_block' => 0,
      'views_bulk_operations_delete_user_action' => 0,
      'system_message_action' => 0,
      'views_bulk_operations_action' => 0,
      'views_bulk_operations_script_action' => 0,
      'views_bulk_operations_user_roles_action' => 0,
      'views_bulk_operations_argument_selector_action' => 0,
      'system_goto_action' => 0,
      'vr_list_user_subscribe' => 0,
      'system_send_email_action' => 0,
      'user_user_operations_unblock' => 0,
    ),
  ));
  $handler->override_option('path', 'node/%/enews-export/vr-recipients-review');
  $handler->override_option('menu', array(
    'type' => 'tab',
    'title' => 'Review Recipients',
    'description' => '',
    'weight' => '2',
    'name' => 'navigation',
  ));
  $handler->override_option('tab_options', array(
    'type' => 'none',
    'title' => '',
    'description' => '',
    'weight' => 0,
    'name' => 'navigation',
  ));
  
  $views[$view->name] = $view;
  return $views;
}